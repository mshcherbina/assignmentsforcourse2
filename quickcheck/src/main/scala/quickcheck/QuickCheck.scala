package quickcheck

import common._

import org.scalacheck._
import Arbitrary._
import Gen._
import Prop._

abstract class QuickCheckHeap extends Properties("Heap") with IntHeap {

  lazy val genHeap: Gen[H] = oneOf(
    const(empty),
    for {
      k <- arbitrary[Int]
      m <- oneOf(const(empty), genHeap)
    } yield insert(k,m)
  )
  implicit lazy val arbHeap: Arbitrary[H] = Arbitrary(genHeap)

  property("gen1") = forAll { (h: H) =>
    val m = if (isEmpty(h)) 0 else findMin(h)
    findMin(insert(m, h)) == m
  }

  property("min1") = forAll { a: Int =>
    val h = insert(a, empty)
    findMin(h) == a
  }

  property("oneElem") = forAll { a: Int =>
    val h = insert(a, empty)
    deleteMin(h) == empty
  }

  def min2(a:Int, b:Int):Int ={
    if (a>=b) b else a
  }
  def max2(a:Int, b:Int):Int ={
    if (a<=b) b else a
  }
  property("minOfMergingTrees") = forAll { h1: H =>
    forAll{ h2: H =>
      if (!isEmpty(h1) && !isEmpty(h2)) {
        min2(findMin(h1), findMin(h2))==findMin(meld(h1,h2))
      }
      else if (isEmpty(h1) && !isEmpty(h2)) findMin(h2)==findMin(meld(h1,h2))
      else if (isEmpty(h2) && !isEmpty(h1)) findMin(h1)==findMin(meld(h1,h2))
      else true
     }
  }
  property("minOf2Elem") = forAll { a: Int =>
    forAll { b: Int =>
      val h = insert(b,insert(a, empty))
      findMin(h)==min2(a,b)
    }
  }

  def GetListFromTree(h: H): List[Int] = if (isEmpty(h)) List()
      else findMin(h)::GetListFromTree(deleteMin(h))


  property("SeqOrd") = forAll { h: H =>
    val tmp=GetListFromTree(h)
    tmp == tmp.sorted
  }
  property("minOf2Elem") = forAll { a: Int =>
    forAll { b: Int =>
      val h = insert(b,insert(a, empty))
      deleteMin(h) == insert(max2(a,b), empty)
    }
  }

  property("minOf3Elem") = forAll { a: Int =>
    forAll { b: Int => forAll{c: Int =>
      val h = insert(c,insert(b,insert(a, empty)))
      deleteMin(deleteMin(h)) == insert(max2(max2(a,b),c), empty)
    }
  }}
}
